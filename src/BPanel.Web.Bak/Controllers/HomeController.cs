using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using BPanel.Web.Models;
using Microsoft.AspNetCore.Authorization;

namespace BPanel.Web.Controllers;

[Authorize]
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    /// <summary>
    /// HomeController
    /// </summary>
    /// <param name="logger"></param>
    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        ViewData["menu"] = "home";
        return View();
    }
}