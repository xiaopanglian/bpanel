﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BPanel.Web.Controllers;

[Authorize]
public class WebSiteController : Controller
{
    public IActionResult Index()
    {
        ViewData["menu"] = "website";
        
        return View();
    }

    public IActionResult Add()
    {
        ViewData["menu"] = "website";
        
        return View();
    }
}