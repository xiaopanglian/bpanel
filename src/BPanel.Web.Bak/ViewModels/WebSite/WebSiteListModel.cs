﻿namespace BPanel.Web.ViewModels.WebSite;

public class WebSiteListModel
{
    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 网站名称
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 根目录
    /// </summary>
    public string RootDirectory { get; set; } = string.Empty;

    /// <summary>
    /// 站点端口
    /// </summary>
    public int Port { get; set; }
}