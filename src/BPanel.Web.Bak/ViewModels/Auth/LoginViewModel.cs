﻿namespace BPanel.Web.ViewModels.Auth;

public class LoginViewModel
{
    /// <summary>
    /// 用户
    /// </summary>
    public string Username { get; set; } = string.Empty;

    /// <summary>
    /// 授权Token
    /// </summary>
    public string Token { get; set; } = string.Empty;
}