﻿using System.ComponentModel.DataAnnotations;

namespace BPanel.Web.ViewModels.Auth;

public class LoginModel
{
    /// <summary>
    /// 用户名
    /// </summary>
    [Required(ErrorMessage = "用户名不能为空")]
    [MaxLength(ErrorMessage = "用户名长度不能超过20个字符")]
    public string Username { get; set; } = string.Empty;

    /// <summary>
    /// 密码
    /// </summary>
    [Required(ErrorMessage = "密码不能为空")]
    [MaxLength(ErrorMessage = "密码长度不能超过20个字符")]
    public string Password { get; set; } = string.Empty;

    /// <summary>
    /// 回调地址
    /// </summary>
    public string? ReturnUrl { get; set; }

    /// <summary>
    /// 记住我
    /// </summary>
    public bool RememberMe { get; set; }
}