﻿namespace BPanel.Web.ViewModels.System;

/// <summary>
/// 系统设置
/// </summary>
public class SystemSettingModel
{
    /// <summary>
    /// 站点标题
    /// </summary>
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// 默认建站目录
    /// </summary>
    public string DefaultSiteDirectory { get; set; } = string.Empty;

    /// <summary>
    /// 面板端口
    /// </summary>
    public int Port { get; set; }
}