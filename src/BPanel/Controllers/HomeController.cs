﻿using Microsoft.AspNetCore.Mvc;

namespace BPanel.Controllers;

/// <summary>
/// 首页，给前端预留路由，直接跳转vue页面
/// </summary>
[Route("")]
public class HomeController : Controller
{
    /// <summary>
    /// 跳转vue首页
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }
}