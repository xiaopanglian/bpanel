﻿namespace BPanel.ViewModels.WebSite;

/// <summary>
/// 删除站点
/// </summary>
public class DeleteWebSiteModel
{
    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 是否删除数据库
    /// </summary>
    public bool DeleteDatabase { get; set; } = false;
}