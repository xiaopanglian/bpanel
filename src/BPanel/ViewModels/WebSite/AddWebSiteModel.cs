﻿using BPanel.Database.Enums;

namespace BPanel.ViewModels.WebSite;

/// <summary>
/// 添加站点
/// </summary>
public class AddWebSiteModel
{
    /// <summary>
    /// 初始化数据
    /// </summary>
    public AddWebSiteModel()
    {
        Domains = new List<string>();
    }

    /// <summary>
    /// 网站名称
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 站点类型
    /// </summary>
    public EnumSiteType SiteType { get; set; } = EnumSiteType.PHP;

    /// <summary>
    /// Php版本
    /// </summary>
    public EnumPhpVersion? PhpVersion { get; set; }

    /// <summary>
    /// 默认端口
    /// </summary>
    public int Port { get; set; } = 80;

    /// <summary>
    /// 域名列表
    /// </summary>
    public List<string> Domains { get; set; }

    /// <summary>
    /// 协议
    /// </summary>
    public EnumProtocol Protocol { get; set; } = EnumProtocol.Http;

    /// <summary>
    /// 过期时间
    /// </summary>
    public DateTime? ExpiredTime { get; set; }

    /// <summary>
    /// 是否创建数据库
    /// </summary>
    public bool CreateDatabase { get; set; }

    public void Check()
    {
        if (SiteType == EnumSiteType.PHP && PhpVersion == null)
            throw new Exception("PHP站点必须选择PHP版本号");
    }
}