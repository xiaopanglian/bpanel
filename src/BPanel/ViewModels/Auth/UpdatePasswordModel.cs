﻿using System.ComponentModel.DataAnnotations;

namespace BPanel.ViewModels.Auth;

/// <summary>
/// 修改密码
/// </summary>
public class UpdatePasswordModel
{
    /// <summary>
    /// 旧密码
    /// </summary>
    [Required(ErrorMessage = "旧密码不能为空")]
    public string OldPassword { get; set; } = string.Empty;

    /// <summary>
    /// 新密码
    /// </summary>
    [Required(ErrorMessage = "新密码不能为空")]
    public string NewPassword { get; set; } = string.Empty;

    /// <summary>
    /// 确认新密码
    /// </summary>
    [Required(ErrorMessage = "确认新密码不能为空")]
    public string ReconfirmNewPassword { get; set; } = string.Empty;
}