﻿namespace BPanel.ViewModels.System;

public class InitModel
{
    /// <summary>
    /// 账号
    /// </summary>
    public string Username { get; set; } = string.Empty;

    /// <summary>
    /// 密码
    /// </summary>
    public string Password { get; set; } = string.Empty;
}