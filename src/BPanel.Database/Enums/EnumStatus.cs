﻿namespace BPanel.Database.Enums;

public enum EnumStatus
{
    Stop = 0,
    Running = 1,
    Parse = 2
}