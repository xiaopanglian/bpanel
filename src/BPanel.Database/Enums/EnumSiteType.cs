﻿namespace BPanel.Database.Enums;

public enum EnumSiteType
{
    /// <summary>
    /// PHP站点
    /// </summary>
    PHP = 0,
    
    /// <summary>
    /// 静态站点
    /// </summary>
    Static = 1,
    
    /// <summary>
    /// 反向代理站点
    /// </summary>
    Proxy = 2,
    
    /// <summary>
    /// NodeJS站点
    /// </summary>
    NodeJS = 3
}