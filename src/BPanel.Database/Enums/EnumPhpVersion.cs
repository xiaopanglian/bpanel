﻿using System.ComponentModel;

namespace BPanel.Database.Enums;

/// <summary>
/// PHP版本枚举
/// </summary>
public enum EnumPhpVersion
{
    [Description("PHP-52")] PHP52 = 52,
    [Description("PHP-53")] PHP53 = 53,
    [Description("PHP-54")] PHP54 = 54,
    [Description("PHP-55")] PHP55 = 55,
    [Description("PHP-56")] PHP56 = 56,
    [Description("PHP-70")] PHP70 = 70,
    [Description("PHP-71")] PHP71 = 71,
    [Description("PHP-72")] PHP72 = 72,
    [Description("PHP-73")] PHP73 = 73,
    [Description("PHP-74")] PHP74 = 74,
    [Description("PHP-80")] PHP80 = 80,
    [Description("PHP-81")] PHP81 = 81,
    [Description("PHP-82")] PHP82 = 82,
    [Description("PHP-83")] PHP83 = 83
}