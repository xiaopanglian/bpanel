﻿namespace BPanel.Database.Enums;

public enum EnumProtocol
{
    Http = 0,
    
    Https = 1
}