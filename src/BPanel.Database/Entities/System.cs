﻿using BPanel.Core.Database;
using FreeSql.DataAnnotations;

namespace BPanel.Database.Entities;

/// <summary>
/// 系统表
/// </summary>
[Table(Name = "system")]
public class System : BaseEntity
{
    /// <summary>
    /// 站点标题
    /// </summary>
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// 默认建站目录
    /// </summary>
    public string DefaultSiteDirectory { get; set; } = string.Empty;

    /// <summary>
    /// 面板账号
    /// </summary>
    public string Username { get; set; } = string.Empty;

    /// <summary>
    /// 面板密码
    /// </summary>
    public string Password { get; set; } = string.Empty;

    /// <summary>
    /// 密码锁定时间
    /// </summary>
    public DateTime LockTime { get; set; }

    /// <summary>
    /// 异常次数
    /// </summary>
    public int ErrorCount { get; set; }

    /// <summary>
    /// 面板版本
    /// </summary>
    public string Version { get; set; } = string.Empty;

    /// <summary>
    /// 面板端口
    /// </summary>
    public int Port { get; set; }
}