﻿using BPanel.Core.Database;
using BPanel.Database.Enums;

namespace BPanel.Database.Entities;

/// <summary>
/// 已安装PHP版本
/// </summary>
public class PhpVersion : BaseEntity
{
    /// <summary>
    /// Php版本
    /// </summary>
    public EnumPhpVersion Version { get; set; } = EnumPhpVersion.PHP74;

    /// <summary>
    /// 安装目录
    /// </summary>
    public string RootDirectory { get; set; } = string.Empty;

    /// <summary>
    /// 安装时间
    /// </summary>
    public DateTime InstallTime { get; set; }
}