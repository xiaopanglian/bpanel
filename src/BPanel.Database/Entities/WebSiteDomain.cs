﻿using BPanel.Core.Database;
using FreeSql.DataAnnotations;

namespace BPanel.Database.Entities;

/// <summary>
/// 网站域名
/// </summary>
[Table(Name = "website-domain")]
public class WebSiteDomain : BaseEntity
{
    /// <summary>
    /// 站点Id
    /// </summary>
    public long WebSiteId { get; set; }

    /// <summary>
    /// 域名
    /// </summary>
    public string Domain { get; set; } = string.Empty;
}