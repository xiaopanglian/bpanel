﻿using BPanel.Core.Database;
using BPanel.Database.Enums;
using FreeSql.DataAnnotations;

namespace BPanel.Database.Entities;

/// <summary>
/// 数据库
/// </summary>
[Table(Name = "database")]
public class Database : BaseEntity
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public EnumDatabaseType DatabaseType { get; set; }

    /// <summary>
    /// 数据库名称
    /// </summary>
    public string DatabaseName { get; set; } = string.Empty;

    /// <summary>
    /// 用户名
    /// </summary>
    public string Username { get; set; } = string.Empty;

    /// <summary>
    /// 密码
    /// </summary>
    public string Password { get; set; } = string.Empty;

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }
}