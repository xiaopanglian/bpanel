﻿using BPanel.Core.Database;
using BPanel.Database.Enums;
using FreeSql.DataAnnotations;

namespace BPanel.Database.Entities;

/// <summary>
/// 站点
/// </summary>
[Table(Name = "website")]
public class WebSite : BaseEntity
{
    /// <summary>
    /// 网站名称
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 站点类型
    /// </summary>
    public EnumSiteType SiteType { get; set; } = EnumSiteType.PHP;

    /// <summary>
    /// PHP版本
    /// </summary>
    /// <summary>PHP版本。只有站点类型为PHP站点时，才有值</summary>
    public EnumPhpVersion? PhpVersion { get; set; }

    /// <summary>
    /// 状态
    /// </summary>
    public EnumStatus Status { get; set; } = EnumStatus.Stop;

    /// <summary>
    /// 端口
    /// </summary>
    public int Port { get; set; }

    /// <summary>
    /// 根目录
    /// </summary>
    public string RootDirectory { get; set; } = string.Empty;

    /// <summary>
    /// 协议
    /// </summary>
    public EnumProtocol Protocol { get; set; } = EnumProtocol.Http;

    /// <summary>
    /// 过期时间
    /// </summary>
    public DateTime? ExpiredTime { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }
}