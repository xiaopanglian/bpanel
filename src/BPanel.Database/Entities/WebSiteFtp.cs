﻿using BPanel.Core.Database;
using FreeSql.DataAnnotations;

namespace BPanel.Database.Entities;

/// <summary>
/// 网站FTP信息
/// </summary>
[Table(Name = "website-ftp")]
public class WebSiteFtp : BaseEntity
{
    /// <summary>
    /// 网站Id
    /// </summary>
    public long WebSiteId { get; set; }
}