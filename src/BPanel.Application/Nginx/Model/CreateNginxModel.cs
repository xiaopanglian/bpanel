﻿using BPanel.Database.Enums;

namespace BPanel.Application.Nginx.Model;

public class CreateNginxModel
{
    /// <summary>
    /// 
    /// </summary>
    public CreateNginxModel()
    {
        Domains = new List<string>();
    }

    /// <summary>
    /// 站点名称
    /// </summary>
    public string Name { get; set; } = String.Empty;

    /// <summary>
    /// 站点类型
    /// </summary>
    public EnumSiteType SiteType { get; set; }

    /// <summary>
    /// PHP版本
    /// </summary>
    public EnumPhpVersion? PhpVersion { get; set; }

    /// <summary>
    /// 站点端口
    /// </summary>
    public int Port { get; set; }

    /// <summary>
    /// 域名列表
    /// </summary>
    public List<string> Domains { get; set; }


    public string GetDomainServerString()
    {
        if (Domains == null || Domains.Count == 0)
            return string.Empty;
        return string.Join(' ', Domains);
    }
}