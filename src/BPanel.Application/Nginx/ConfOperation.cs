﻿using System.Text;
using BPanel.Application.Nginx.Model;
using BPanel.Application.Shell.Php;
using BPanel.Core.Shell;
using BPanel.Database.Enums;

namespace BPanel.Application.Nginx;

/// <summary>
/// Nginx配置操作类
/// </summary>
public static class ConfOperation
{
    public static void CreateWebSite(CreateNginxModel model)
    {
        var conf = new StringBuilder();
        // 拼接站点nginx配置内容
        conf.AppendLine("server {");

        conf.AppendLine($"\tlisten {model.Port};");
        conf.AppendLine($"\tserver_name {model.GetDomainServerString()};");

        {
            // location 配置
            conf.AppendLine("\tlocation / {");
            conf.AppendLine($"\t\troot /www/wwwroot/{model.Name}/index;");
            var index = " index.html index.htm";
            if (model.SiteType == EnumSiteType.PHP)
            {
                index += " index.php";
            }

            conf.AppendLine($"\t\tindex {index};");
            conf.AppendLine("\t}");
        }

        // PHP配置
        if (model.SiteType == EnumSiteType.PHP)
        {
            var phpInfo = PhpConstConfig.GetPhpInstallPackageDownloadUrl(model.PhpVersion ?? EnumPhpVersion.PHP74);
            conf.Append($@"
location ~ \.php$ {{
   include fastcgi.conf;
   fastcgi_pass unix:/var/run/php{phpInfo.VersionName}-fpm.sock;
   fastcgi_index index.php;
}}
");
        }

        conf.AppendLine("}");

        // 写入新的配置
        // 使用 using 语句确保文件流最终会被正确关闭
        var filePath = $"/usr/local/nginx/conf/sites/{model.Name}.conf";
        using var writer = new StreamWriter(filePath, false, Encoding.UTF8);
        writer.Write(conf.ToString());

        // 创建站点目录
        var sce = new ShellCommandExecutor();
        sce.ExecuteCommand($"mkdir -p /www/wwwroot/{model.Name}/ssl"); // ssl配置目录
        sce.ExecuteCommand($"mkdir -p /www/wwwroot/{model.Name}/logs"); // 站点日志
        sce.ExecuteCommand($"mkdir -p /www/wwwroot/{model.Name}/index"); // 站点程序主目录

        // 写入站点默认index.html文件
        var indexFilePath = $"/www/wwwroot/{model.Name}/index/index.html";
        using var indexWriter = new StreamWriter(indexFilePath, false, Encoding.UTF8);
        indexWriter.Write($"站点创建成功...");
    }

    /// <summary>
    /// nginx重新加载配置
    /// </summary>
    public static void NginxReload()
    {
        var sce = new ShellCommandExecutor();
        sce.ExecuteCommand("nginx -s reload");
    }

    /// <summary>
    /// 删除nginx配置文件
    /// </summary>
    /// <param name="name"></param>
    public static void DeleteNginxConf(string name)
    {
        var filePath = $"/usr/local/nginx/conf/sites/{name}.conf";

        var sce = new ShellCommandExecutor();
        sce.ExecuteCommand($"rm {filePath}");
    }

    /// <summary>
    /// 删除站点根目录
    /// </summary>
    /// <param name="name"></param>
    public static void DeleteWebsiteWwwRoot(string name)
    {
        var filePath = $"/www/wwwroot/{name}";

        var sce = new ShellCommandExecutor();
        sce.ExecuteCommand($"rm -r {filePath}");
    }
}