﻿namespace BPanel.Application.Shell.Php.Model;

public class PhpInstallPackageInfo
{
    public string DownloadUrl { get; set; } = string.Empty;
    public string DirectoryName { get; set; } = string.Empty;
    public string FileName { get; set; } = string.Empty;
    public string VersionName { get; set; } = string.Empty;
    public int VersionNumber { get; set; }
}