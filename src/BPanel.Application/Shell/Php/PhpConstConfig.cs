﻿using BPanel.Application.Shell.Php.Model;
using BPanel.Database.Enums;

namespace BPanel.Application.Shell.Php;

public static class PhpConstConfig
{
    /// <summary>
    /// 获取php安装包下载地址
    /// </summary>
    /// <param name="phpVersion"></param>
    /// <returns></returns>
    public static PhpInstallPackageInfo GetPhpInstallPackageDownloadUrl(EnumPhpVersion phpVersion)
    {
        return phpVersion switch
        {
            EnumPhpVersion.PHP80 => new PhpInstallPackageInfo(){ DownloadUrl = "https://www.php.net/distributions/php-8.0.30.tar.gz", DirectoryName = "php-8.0.30", FileName = "php-8.0.30.tar.gz", VersionName = "8.0", VersionNumber = 80},
            _ => throw new Exception("PHP版本不支持")
        };
    }
}