﻿using BPanel.Core.Shell;
using BPanel.Database.Enums;

namespace BPanel.Application.Shell.Php;

public class PhpOperation
{
    /// <summary>
    /// 安装Php指定版本
    /// </summary>
    /// <param name="phpVersion"></param>
    public static void InstallPhp(EnumPhpVersion phpVersion)
    {
        var shellCommand = new ShellCommandExecutor();

        ExcuteDependency();

        // 进入面板PHP目录
        shellCommand.ExecuteCommand("cd /www/server/php");
        // #先下载php版本包
        var phpInfo = PhpConstConfig.GetPhpInstallPackageDownloadUrl(phpVersion);
        shellCommand.ExecuteCommand("wget " + phpInfo.DownloadUrl);
        // 解压
        shellCommand.ExecuteCommand("tar -xzf " + phpInfo.FileName);
        // 进入php版本目录
        shellCommand.ExecuteCommand("cd " + phpInfo.DirectoryName);

        // 配置PHP
        shellCommand.ExecuteCommand($@"
./configure --prefix=/usr/local/php{phpInfo.VersionNumber} --enable-fpm --enable-opcache \
--with-zlib \
            --with-pdo-mysql \
            --with-mysqli \
            --with-curl \
            --with-gd \
            --with-openssl \
            --with-mhash \
            --with-xmlrpc \
            --with-xsl \
            --enable-mbstring \
            --enable-soap \
            --enable-calendar \
            --enable-zip \
            --enable-bcmath \
            --enable-sockets \
            --enable-intl \
            --enable-exif
");
        // 安装
        shellCommand.ExecuteCommand("make");
        shellCommand.ExecuteCommand("make install");

        // 复制配置文件
        shellCommand.ExecuteCommand($"cp /usr/local/php{phpInfo.VersionName}/etc/php-fpm.conf.default /usr/local/php{phpInfo.VersionName}/etc/php-fpm.conf");
        shellCommand.ExecuteCommand($"cp /usr/local/php{phpInfo.VersionName}/etc/php-fpm.d/www.conf.default /usr/local/php{phpInfo.VersionName}/etc/php-fpm.d/www.conf");

        // 编辑/usr/local/php8.0/etc/php-fpm.d/www.conf，修改监听方式为sock
        // listen = /var/run/php8.0-fpm.sock
        shellCommand.ExecuteCommand("sed -i -E '0,/^listen =.*/s//listen = \\/var\\/run\\/php8.0-fpm.sock/' \"/usr/local/php80/etc/php-fpm.d/www.conf\"");

        // 启动PHP-FPM
        shellCommand.ExecuteCommand($"/usr/local/php{phpInfo.VersionNumber}/sbin/php-fpm");

        // 将PHP-FPM添加到系统服务中以便开机自启动。创建systemd服务文件
        // nano /etc/systemd/system/php8.0-fpm.service

        // 添加服务文件内容
        /*
         [Unit]
Description=The PHP 8.0 FastCGI Process Manager
After=network.target

[Service]
Type=simple
PIDFile=/run/php/php8.0-fpm.pid
ExecStart=/usr/local/php8.0/sbin/php-fpm --nodaemonize --fpm-config /usr/local/php8.0/etc/php-fpm.conf
ExecReload=/bin/kill -USR2 $MAINPID
ExecStop=/bin/kill -TERM $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
         */

        // 保存并退出，然后重新加载systemd并启动服务
/*
 sudo systemctl daemon-reload
sudo systemctl start php8.0-fpm
sudo systemctl enable php8.0-fpm
 */
    }

    /// <summary>
    /// 移除Php指定版本
    /// </summary>
    /// <param name="phpVersion"></param>
    public static void RemovePhp(EnumPhpVersion phpVersion)
    {
    }

    /// <summary>
    /// 安装Php依赖
    /// </summary>
    private static void ExcuteDependency()
    {
        var shellCommand = new ShellCommandExecutor();
        shellCommand.ExecuteCommand("apt-get update");
        shellCommand.ExecuteCommand("apt-get install -y make libxml2-dev libssl-dev libpng-dev zip unzip pkg-config sqlite3 libsqlite3-dev libonig-dev libxslt-dev build-essential zlib1g-dev libcurl4-openssl-dev libreadline-dev libjpeg-dev libfreetype6-dev libmcrypt-dev libiconv-hook-dev");
    }
}