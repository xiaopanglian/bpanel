﻿using System.Text;

namespace BPanel.Core.Extensions;

public static class CryptExtension
{
    /// <summary>
    /// md5加密
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string Md5(this string str)
    {
        using var md5 = System.Security.Cryptography.MD5.Create();
        var inputBytes = Encoding.ASCII.GetBytes(str);
        var hashBytes = md5.ComputeHash(inputBytes);

        var sb = new StringBuilder();
        foreach (var item in hashBytes)
        {
            sb.Append(item.ToString("X2"));
        }

        return sb.ToString();
    }
}