﻿namespace BPanel.Core.Extensions.Models;

public class KeyValueModel
{
    public string Key { get; set; } = string.Empty;
    public int Value { get; set; }
}