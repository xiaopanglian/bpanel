﻿namespace BPanel.Core.Encrypt;

public static class BuildHelper
{
    public static string GetRandomString(int length)
    {
        var returnString = string.Empty;
        var baseString = "abcdefghijklmnopqrstuvwxyz1234567890,./<>?;':!@#$%^&*()_+-=";

        for (int i = 0; i < length; i++)
        {
            returnString += baseString[new Random().Next(0, baseString.Length)];
        }

        return returnString;
    }
}