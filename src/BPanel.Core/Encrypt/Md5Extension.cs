﻿using System.Security.Cryptography;
using System.Text;

namespace BPanel.Core.Encrypt;

public static class Md5Extension
{
    /// <summary>
    /// 转换为MD5.返回小写的MD5字符串
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ToMd5String(this string input)
    {
        // 创建一个新的 MD5 对象
        using (MD5 md5 = MD5.Create())
        {
            // 将输入字符串转换为字节
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);

            // 计算输入字符串的 MD5 哈希值
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // 将哈希值转换为十六进制字符串
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2").ToLower());
            }

            return sb.ToString();
        }
    }
}