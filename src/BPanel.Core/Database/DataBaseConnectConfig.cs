﻿using System.Data;
using System.Data.SQLite;
using Microsoft.Extensions.Configuration;

namespace BPanel.Core.Database;

/// <summary>
/// 数据库连接类
/// </summary>
public static class DataBaseConnectConfig
{
    /// <summary>
    /// 连接数据库
    /// </summary>
    /// <param name="configuration"></param>
    /// <param name="sqlConnectionStr">连接数据库字符串</param>
    /// <returns></returns>
    public static SQLiteConnection GetSqlConnection(this IConfiguration configuration, string? sqlConnectionStr = null)
    {
        if (string.IsNullOrWhiteSpace(sqlConnectionStr))
        {
            sqlConnectionStr = configuration.GetValue<string>("ConnectionStrings:SqlConnection");
        }

        var connection = new SQLiteConnection(sqlConnectionStr);
        if (connection.State != ConnectionState.Open)
        {
            connection.Open();
        }

        return connection;
    }
}