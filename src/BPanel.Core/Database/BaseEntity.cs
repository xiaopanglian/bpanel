﻿using System.ComponentModel;
using FreeSql.DataAnnotations;

namespace BPanel.Core.Database;

/// <summary>
/// 基础
/// </summary>
public class BaseEntity
{
    /// <summary>
    /// 主键Id
    /// </summary>
    [Column(IsPrimary = true, Name = "id")]
    [Description("主键Id")]
    public long Id { get; set; }
}