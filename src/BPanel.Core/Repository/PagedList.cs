﻿namespace BPanel.Core.Repository;

public class PagedList<T>
{
    public List<T> Data { get; set; }

    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int Total { get; set; }

    public PagedList(List<T> list, int total, int pageIndex, int pageSize)
    {
        Data = list;
        Total = total;
        PageIndex = pageIndex;
        PageSize = pageSize;
    }
}