﻿using BPanel.Core.Repository;

namespace BPanel.Core.Middlewares;

public class ApiResponse
{
    public ApiResponse()
    {
    }

    public bool Success { get; set; }
    public int StatusCode { get; set; } = 0;
    public string Message { get; set; } = "";

    public ApiResponse(bool success, string message = "Success", int statusCode = 0)
    {
        Success = success;
        Message = message;
        StatusCode = statusCode;
    }

    public static ApiResponse Ok()
    {
        return new ApiResponse() { Success = true };
    }

    public static ApiResponse<T> Ok<T>(T tModel)
    {
        return new ApiResponse<T>(tModel);
    }

    public static ApiResponse<PagedList<T>> Ok<T>(List<T> tList, int total, int pageIndex, int pageSize)
    {
        return new ApiResponse<PagedList<T>>(new PagedList<T>(tList, total, pageIndex, pageSize));
    }

    public static ApiResponse Error(string message)
    {
        return new ApiResponse { Success = false, Message = message, StatusCode = -1 };
    }

    public static ApiResponse<T> Error<T>(T tModel, string message)
    {
        return new ApiResponse<T>(tModel) { Success = false, Message = message, StatusCode = -1 };
    }
}

public class ApiResponse<T> : ApiResponse
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="tModel"></param>
    public ApiResponse(T tModel)
    {
        Data = tModel;
        Success = true;
    }

    public T Data { get; set; }
}

public class ApiResponses<T> : ApiResponse
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="tModel"></param>
    public ApiResponses(T tModel)
    {
        Data = tModel;
        Success = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="tModel"></param>
    /// <param name="total"></param>
    /// <param name="pageIndex"></param>
    /// <param name="pageSize"></param>
    public ApiResponses(T tModel, long total, int pageIndex, int pageSize)
    {
        Success = true;
        Data = tModel;
        Total = total;
        PageIndex = pageIndex;
        PageSize = pageSize;
    }

    public T Data { get; set; }

    public long Total { get; set; }

    public int PageIndex { get; set; }

    public int PageSize { get; set; }
}