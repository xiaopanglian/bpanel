﻿using System.Diagnostics;

namespace BPanel.Core.Shell;

public class ShellCommandExecutor
{
    /// <summary>
    /// 执行命令。注：不能含有双引号
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public string ExecuteCommand(string command)
    {
        try
        {
            // 创建一个新的 ProcessStartInfo 对象
            var processStartInfo = new ProcessStartInfo
            {
                FileName = "/bin/sh", // Linux 的 shell
                Arguments = "-c \"" + command + "\"", // 传递命令作为参数
                UseShellExecute = false, // 不使用 shell 来启动进程
                RedirectStandardOutput = true, // 重定向标准输出
                RedirectStandardError = true, // 重定向标准错误
                // CreateNoWindow = true // 不创建新窗口
            };

            // 创建并启动进程
            using var process = Process.Start(processStartInfo);
            if (process == null)
                return string.Empty;
            // 读取标准输出
            var output = process.StandardOutput.ReadToEnd();
            // 读取标准错误
            var error = process.StandardError.ReadToEnd();

            // 等待进程退出
            process.WaitForExit();

            // 如果有错误输出，抛出异常
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception("命令错误: " + error);
            }

            return output;
        }
        catch
        {
            return string.Empty;
        }
    }
}