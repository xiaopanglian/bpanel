#!/bin/bash

PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:$PATH

OS=$(source /etc/os-release && { [[ "$ID" == "debian" ]] || [[ "$ID_LIKE" =~ "debian" ]] && echo "debian"; } || { [[ "$ID" == "ubuntu" ]] || [[ "$ID_LIKE" =~ "ubuntu" ]] && echo "ubuntu"; } || { [[ "$ID" == "tencentos" ]] || [[ "$ID" == "opencloudos" ]] || [[ "$ID" == "hce" ]] || [[ "$ID" == "openEuler" ]] || [[ "$ID" == "rhel" ]] || [[ "$ID_LIKE" =~ "rhel" ]] && echo "rhel"; } || echo "unknown")
CORES=$(nproc)
MEM=$(LC_ALL=C free -m | grep Mem | awk '{print  $2}')
SWAP=$(LC_ALL=C free -m | grep Swap | awk '{print  $2}')

ssh_port=$(cat /etc/ssh/sshd_config | grep 'Port ' | awk '{print $2}')
in_china=$(curl --retry 2 -m 10 -L https://www.cloudflare-cn.com/cdn-cgi/trace 2>/dev/null | grep -qx 'loc=CN' && echo "true" || echo "false")

IP=127.0.0.1
####################################################   CLI TOOLS   ###

reset=$(tput sgr0)
bold=$(tput bold)
underline=$(tput smul)
black=$(tput setaf 0)
white=$(tput setaf 7)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
purple=$(tput setaf 5)
bgblack=$(tput setab 0)
bgwhite=$(tput setab 7)
bgred=$(tput setab 1)
bggreen=$(tput setab 2)
bgyellow=$(tput setab 4)
bgblue=$(tput setab 4)
bgpurple=$(tput setab 5)

####################################################    配置    ###

# nginx安装包链接
nginx_url="http://nginx.org/download/nginx-1.26.2.tar.gz"
# 下载包存放路径
path="/tmp/"
# 安装路径
int_path="/usr/local/nginx"

# 安装依赖包
function dependOn(){
	clear
	echo "${bggreen}${black}${bold}"
	echo "正在安装依赖包..."
	echo "${reset}"
	sleep 1
	
	sudo apt-get update
	sudo apt-get install -y build-essential libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev software-properties-common curl wget 
}

# 安装nginx
function install-nginx(){
	clear
	# 下载Nginx包
	echo "${bggreen}${black}${bold}"
	echo "正在安装Nginx..."
	echo "${reset}"
	sleep 1
	
	echo "下载nginx安装包"
	wget $nginx_url -P $path > /dev/null 2>&1
	# 创建文件夹，解压安装
	mkdir $int_path && cd $int_path
	nginx_pack=`echo $nginx_url | awk -F '/' '{print $NF}'`
	tar -xf $path/$nginx_pack -C ./
	
	# 编译安装
	echo "编译安装nginx"
	nginx_path=`echo $nginx_pack |awk -F '.' '{print $1"."$2"."$3}'`
	cd $nginx_path && ./configure > /dev/null 2>&1
	make > /dev/null 2>&1
	make install  > /dev/null 2>&1
	# nginx配置文件添加其他配置文件引用
	echo "更新nginx配置"
	sudo sed -i '/http {/a\\ include /usr/local/nginx/conf/sites/*.conf;' /usr/local/nginx/conf/nginx.conf
	# 创建站点配置目录
	mkdir -p /usr/local/nginx/conf/sites
	# 创建站点程序目录
	mkdir -p /www/wwwroot
	# 添加环境变量
	echo 'PATH=$PATH:/usr/local/nginx/sbin' >> /etc/profile
	# 重启生效
	source /etc/profile
	# 启动nginx
	nginx
	echo "${bggreen}${black}${bold}"
	echo "Nginx 安装已完成！"
	echo "${reset}"
	echo -e "#####################################\n启动Nginx: $int_path/sbin/nginx\n停止Nginx: $int_path/sbin/nginx -s stop\n重载Nginx: $int_path/sbin/nginx -s reload\n检查Nginx: $int_path/sbin/nginx -t\n#####################################"
	sleep 3
}

# 安装.NET运行时
function install-dotnet-runtime(){
	clear
	echo "${bggreen}${black}${bold}"
	echo "正在安装dotnet runtime..."
	echo "${reset}"
	sleep 1
	
	wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
	sudo dpkg -i packages-microsoft-prod.deb
	rm packages-microsoft-prod.deb
	
	sudo apt-get update && sudo apt-get install -y aspnetcore-runtime-8.0
}


# 安装面板
function install-panel(){
	clear
	echo "${bggreen}${black}${bold}"
	echo "正在安装bpanel面板..."
	echo "${reset}"
	sleep 1
	
	# 下载最新bpanel包
	mkdir -p /www/server/panel
	cd /www/server/panel
	wget https://bpanel.net/latest.zip
	sudo unzip latest.zip
	
	mkdir -p /www/server/logs
	
	CONFIG_FILE="/etc/supervisor/conf.d/bpanel.conf"
	CONFIG_CONTENT="[program:bpanel]
command=dotnet BPanel.dll --urls=http://*:9898
directory=/www/server/panel
user=root
autostart=true
autorestart=true
stderr_logfile=/www/server/logs/bpanel.err.log
stdout_logfile=/www/server/logs/bpanel.out.log
"

	# 将配置内容写入配置文件
	echo "$CONFIG_CONTENT" | sudo tee$CONFIG_FILE > /dev/null
	
	# 更新supervisor配置并启动程序
	sudo supervisorctl reread
	sudo supervisorctl update
	sudo supervisorctl start bpanel
}



# 安装supervisord
function install-supervisord(){
	clear
	echo "${bggreen}${black}${bold}"
	echo "正在安装supervisord..."
	echo "${reset}"
	sleep 1
	
	sudo apt-get -y install supervisor
	service supervisor restart
}


function main(){
	clear
cat << "EOF"

    __    ____                   __
   / /_  / __ \____ _____  ___  / /
  / __ \/ /_/ / __ `/ __ \/ _ \/ / 
 / /_/ / ____/ /_/ / / / /  __/ /  
/_.___/_/    \__,_/_/ /_/\___/_/   
                                   
EOF

	echo "${bggreen}${black}${bold}"
	echo "开始安装bPanel..."
	echo "${reset}"

	sleep 3

	dependOn
	install-nginx
	install-dotnet-runtime
	install-supervisord
	install-panel
	
	clear
	echo "***********************************************************"
	echo "                       安装完成"
	echo "***********************************************************"
	echo "默认访问地址：$IP:9898"
	echo "面板账号：xxxxxx"
	echo "面板密码：xxxxxx"
	echo "---------------------------------"
}

main